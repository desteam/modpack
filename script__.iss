#define MyAppName         "DeSeRtod ModPack 0.9.5 Common Test"                       ;�������� ������������
#define MyAppVer          "2.0"                                          ;������ ����
#define Patch             "0.9.5 Common Test"                                        ;������ ����� (����� �� ������ ����� ������� ����� ����� � �������, ����� ������������ ���)
#define MyAppPublisher    "woole"                                        ;��� �������� ��� �������� ��� �����
#define MyAppURL          "http://www.youtube.com/user/DeSeRtodTV"       ;������ ��� ����

#include "Components.iss"                                           ;����������, ��� �� ����
#include "Messages.iss"                                             ;���������, ������� �� ������� � �.�.
#include "BackUpPage.iss"                                           ;�������� ����������� ��� �������� res_mods
#include "CustomMessages.iss"                                       ;�������� �����

[Setup]
AppId={{061B7081-2AFB-4D31-AE8C-85E36FA433AF}
AppName={#MyAppName}
AppVersion={#Patch}
AppPublisher={#MyAppPublisher}
VersionInfoVersion={#MyAppVer}
VersionInfoCopyright="woole & fleshr"
VersionInfoCompany="DeSeRtodTV"
AppComments="DeSeRtod ModPack {#Patch} ver. {#MyAppVer}"
AppCopyright=Copyright (C) 2014 woole & fleshr

//====={ ������ }=====\\
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}

//====={ ����� �������� }=====\\
DefaultDirName={code:MyDirName}
AppendDefaultDirName=no
DirExistsWarning=no
//CreateUninstallRegKey=no
//Uninstallable=no
UninstallFilesDir={app}\DeSeRtod Mod Pack


//====={ �������� }=====\\
SetupIconFile=Files\icon.ico
WizardImageFile=Files\main.bmp
WizardSmallImageFile=Files\top.bmp

//====={ ���������� ������� }=====\\
DisableProgramGroupPage=yes
DisableReadyMemo=yes
DisableReadyPage=yes
ShowComponentSizes=no
ExtraDiskSpaceRequired=0

//====={ �������� � ��� }=====\\
LicenseFile=licensia.txt
//InfoBeforeFile=faq.rtf

OutputDir=.\Output
OutputBaseFilename=DeSeRtod_ModPack_0.9.5_CT_v2.0

//====={ ������ ������ }=====\\
InternalCompressLevel=max
SolidCompression=true
Compression=zip
//lzma2

[Files]
Source: Files\backup.bmp; Flags: dontcopy noencryption noencryption
Source: Files\save.bmp; Flags: dontcopy noencryption noencryption
Source: Files\delete.bmp; Flags: dontcopy noencryption noencryption
Source: "faq.rtf"; DestDir: "{tmp}"; Flags: dontcopy
DestName: "WizardForm.BitmapImage1.bmp"; Source: "Files\top.bmp"; Flags: dontcopy solidbreak

Source: 7z.exe; Flags: dontcopy
Source: ISTask.dll; Flags: dontcopy


[Code]
Procedure CurPageChanged(CurPageID: Integer);
Begin
	If CurPageID = wpSelectComponents Then
	Begin
		If WizardForm.FindComponent('NextButton') is TButton Then
			TButton(WizardForm.FindComponent('NextButton')).Caption:='���������';
	End;
End;

//====={ ���� ����������� ����� ���� }=====\\
function MyDirName(S:String): String;
var
  InsPath: String;
  er: boolean;
  myFile:String;
begin
  Result:=ExpandConstant('C:\Games\World_of_Tanks\'); //���� ����� ��� �� ����� ������� ����
  er := RegQueryStringValue(HKLM, 'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1', 'InstallLocation', InsPath);
  if er and (InsPath<>'') then //���� ���� ���������� � ��� ���-�� ��������
  begin
    Result := InsPath;
  end;
end;

//====={ �������� �������� ���� }=====\\
function NextButtonClick(CurPageID: Integer): Boolean;
  begin
  Result := True;
    case CurPageID of
    wpSelectDir:
    if not FileExists(ExpandConstant('{app}\WorldOfTanks.exe')) then
      begin
      MsgBox('� �������� ����� �� ������� ����� ����! ������� ������ ����', mbError, MB_OK);
      Result := False;
      end;
  end;

  case CurPageID of wpSelectDir:
    GetVersionNumbersString(ExpandConstant('{app}\WorldOfTanks.exe'), ver);
  end;
  case CurPageID of wpSelectDir:
    if not (ver = '{#Patch}.0') then
      begin
        MsgBox(ExpandConstant('������ �� �������� �� ������ ������ ����.'), mbError, MB_OK);
        Result := False;
        exit;
      end;
  end;
end;

procedure ExtractResources();
var
  ResultCode: Integer;
begin
  WizardForm.StatusLabel.Caption:='���������� ������ ''...';
  CreateDir(ExpandConstant('{app}\res_mods'));
  Exec(ExpandConstant('{app}\DeSeRtod Mod Pack\mods\7z.exe'), 'x *.7z -o..\..\soft -y', '', SW_Show, ewWaitUntilTerminated, ResultCode);
  WizardForm.StatusLabel.Caption:=SetupMessage(msgStatusExtractFiles);
end;

function WotDir(S:String): String;
var
  InsPath: String;
  er: boolean;
begin
  Result:=ExpandConstant('C:\Games\World_of_Tanks\');
  er := RegQueryStringValue(HKLM, 'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1', 'InstallLocation', InsPath);
  if er and (InsPath<>'') then
    Result := InsPath;
end;

function KillTask(ExeFileName: AnsiString): Integer;
  external 'KillTask@files:ISTask.dll stdcall';
function RunTask(FileName: AnsiString; bFullpath: Boolean): Boolean;
  external 'RunTask@files:ISTask.dll stdcall';


//====={ ���� }=====\\
procedure FAQOnClick(Sender: TObject);
var
  Form: TSetupForm;
  Edit: TRichEditViewer;
  CancelButton: TNewButton;
  RTF: AnsiString;
begin
  Form := CreateCustomForm();
  try
    Form.ClientWidth := ScaleX(530);
    Form.ClientHeight := ScaleY(360);
    Form.Caption := '����� ���������� �������';
    Form.CenterInsideControl(WizardForm, False);

  ExtractTemporaryFile('faq.rtf');
  LoadStringFromFile(ExpandConstant('{tmp}\faq.rtf'), RTF);
  Edit := TRichEditViewer.Create(Form);
  with Edit do
  begin
   Parent := Form;
   ScrollBars := ssVertical;
   ReadOnly := True;
   Left := 20;
   Top := 20;
   Width := 490;
   Height := 297;
   RTFText := RTF;
  end;

    CancelButton := TNewButton.Create(Form);
    CancelButton.Parent := Form;
    CancelButton.Width := ScaleX(75);
    CancelButton.Height := ScaleY(23);
    CancelButton.Left := Form.ClientWidth - ScaleX(75 + 19);
    CancelButton.Top := Form.ClientHeight - ScaleY(23 + 10);
    CancelButton.Caption := '�������';
    CancelButton.ModalResult := mrCancel;
    CancelButton.Cancel := True;

    Form.ActiveControl := Edit;

    if Form.ShowModal() = mrOk then
      MsgBox('You clicked OK.', mbInformation, MB_OK);
  finally
    Form.Free();
  end;
end;
procedure CreateTheWizardPages;
var
Button: TNewButton;
begin
  Button := TNewButton.Create(WizardForm);
  Button.Top:=120;
  Button.Left:=410 ;
  Button.Width := ScaleX(75);
  Button.Height := ScaleY(23);
  Button.Caption := 'TNewButton';
  Button.OnClick := @FAQOnClick;
  Button.Parent := WizardForm;
end;

var
  DeleteImg, MoveImg, NoneImg: TBitmapImage;
procedure RedesignWizardForm;
var
  i: integer;
  BtnPanel: TPanel;
  BtnImage: TBitmapImage;
  Button: TNewButton;
begin


begin
  Button := TNewButton.Create(WizardForm);
  Button.Top:=414;
  Button.Left:=254 ;
  Button.Width := ScaleX(75);
  Button.Height := ScaleY(23);
  Button.Caption := 'F.A.Q.';
  Button.OnClick := @FAQOnClick;
  Button.Parent := WizardForm;
end;

  {ExtractTemporaryFile('logo.bmp')
  BtnPanel:=TPanel.Create(WizardForm)
with BtnPanel do begin
  Left:=20
  Top:=410
  Width:=62
  Height:=23
  Cursor:=crHand
  OnClick:=@FAQOnClick
  Parent:=WizardForm
end;
  BtnImage:=TBitmapImage.Create(WizardForm)
with BtnImage do begin
  AutoSize:=True;
  Enabled:=False;
  Bitmap.LoadFromFile(ExpandConstant('{tmp}{')+'\logo.bmp')
  Parent:=BtnPanel
  end;}
  
///
  DeleteImg:= TBitmapImage.Create(WizardForm);
with DeleteImg do
begin
  Parent := WizardForm.SelectDirPage;
  Left := ScaleX(473);
  Top := ScaleY(183);
  Width := ScaleX(32);
  Height := ScaleY(32);
  ExtractTemporaryFile('delete.bmp');
  Bitmap.LoadFromFile(ExpandConstant('{tmp}\delete.bmp'));
  end;
  MoveImg:= TBitmapImage.Create(WizardForm);
with MoveImg do
begin
  Parent := WizardForm.SelectDirPage;
  Left := ScaleX(473);
  Top := ScaleY(228);
  Width := ScaleX(32);
  Height := ScaleY(32);
  ExtractTemporaryFile('backup.bmp');
  Bitmap.LoadFromFile(ExpandConstant('{tmp}\backup.bmp'));
  end;
NoneImg:= TBitmapImage.Create(WizardForm);
with NoneImg do
begin
  Parent := WizardForm.SelectDirPage;
  Left := ScaleX(473);
  Top := ScaleY(273);
  Width := ScaleX(32);
  Height := ScaleY(32);
  ExtractTemporaryFile('save.bmp');
  Bitmap.LoadFromFile(ExpandConstant('{tmp}\save.bmp'));
  end;
///
end;

//====={ ��������� ������� ���� }=====\\
function WindowResize(): Boolean;
var
  HeightOffset, WidthOffset: Integer;
begin
  HeightOffset:=87;
  WidthOffset:=103;

  WizardForm.Height:=WizardForm.Height + HeightOffset;
  WizardForm.Width:=WizardForm.Width + WidthOffset;

  WizardForm.NextButton.Top:=WizardForm.NextButton.Top + HeightOffset;
  WizardForm.BackButton.Top:=WizardForm.BackButton.Top + HeightOffset;
  WizardForm.CancelButton.Top:=WizardForm.CancelButton.Top + HeightOffset;
  WizardForm.NextButton.Left:=WizardForm.NextButton.Left + WidthOffset;
  WizardForm.BackButton.Left:=WizardForm.BackButton.Left + WidthOffset;
  WizardForm.CancelButton.Left:=WizardForm.CancelButton.Left + WidthOffset;

  WizardForm.OuterNotebook.Height:=WizardForm.OuterNotebook.Height + HeightOffset;
  WizardForm.InnerNotebook.Height:=WizardForm.InnerNotebook.Height + HeightOffset;
  WizardForm.OuterNotebook.Width:=WizardForm.OuterNotebook.Width + WidthOffset;
  WizardForm.InnerNotebook.Width:=WizardForm.InnerNotebook.Width + WidthOffset;
  WizardForm.WizardSmallBitmapImage.Left:= WizardForm.WizardSmallBitmapImage.Left + WidthOffset;
  WizardForm.Bevel.Top:=WizardForm.Bevel.Top + HeightOffset;
  WizardForm.BeveledLabel.Top:=WizardForm.BeveledLabel.Top + HeightOffset;
  WizardForm.Bevel.Width:=WizardForm.Bevel.Width + WidthOffset;
  WizardForm.Bevel1.Width:=WizardForm.Bevel1.Width + WidthOffset;
  WizardForm.MainPanel.Width:=WizardForm.MainPanel.Width + WidthOffset;
  WizardForm.BeveledLabel.Width:=WizardForm.BeveledLabel.Width + WidthOffset;
  WizardForm.Center;

  WizardForm.WelcomeLabel1.Hide;
  WizardForm.WelcomeLabel2.Hide;
  WizardForm.WizardBitmapImage.Width:=600;
  WizardForm.WizardBitmapImage.Height:=400;

  WizardForm.FinishedHeadingLabel.Hide;
  WizardForm.FinishedLabel.Hide;
  WizardForm.YesRadio.Hide;
  WizardForm.NoRadio.Hide;
  WizardForm.WizardBitmapImage2.Width:=600;
  WizardForm.WizardBitmapImage2.Height:=400;

  WizardForm.PageNameLabel.Hide;
  WizardForm.PageDescriptionLabel.Hide;
  WizardForm.WizardSmallBitmapImage.Top:=0;
  WizardForm.WizardSmallBitmapImage.Left:=0;
  WizardForm.WizardSmallBitmapImage.Width:=600;
  WizardForm.WizardSmallBitmapImage.Height:=58;

  WizardForm.WelcomeLabel1.Width:=WizardForm.WelcomeLabel1.Width + WidthOffset;
  WizardForm.WelcomeLabel1.Height:=WizardForm.WelcomeLabel1.Height + HeightOffset;
  WizardForm.WelcomeLabel1.Width:=WizardForm.WelcomeLabel1.Width + WidthOffset;

  WizardForm.WelcomeLabel2.Width:=WizardForm.WelcomeLabel2.Width + WidthOffset;
  WizardForm.WelcomeLabel2.Height:=WizardForm.WelcomeLabel2.Height + HeightOffset;
  WizardForm.WelcomeLabel2.Width:=WizardForm.WelcomeLabel2.Width + WidthOffset;


  WizardForm.LicenseLabel1.Width:=WizardForm.LicenseLabel1.Width + WidthOffset;
  WizardForm.LicenseMemo.Height:=WizardForm.LicenseMemo.Height + HeightOffset;
  WizardForm.LicenseMemo.Width:=WizardForm.LicenseMemo.Width + WidthOffset;
  WizardForm.LicenseNotAcceptedRadio.Top:=WizardForm.LicenseNotAcceptedRadio.Top + HeightOffset;
  WizardForm.LicenseAcceptedRadio.Top:=WizardForm.LicenseAcceptedRadio.Top + HeightOffset;

  WizardForm.InfoBeforeClickLabel.Width:=WizardForm.InfoBeforeClickLabel.Width + WidthOffset;
  WizardForm.InfoBeforeMemo.Height:=WizardForm.InfoBeforeMemo.Height + HeightOffset;
  WizardForm.InfoBeforeMemo.Width:=WizardForm.InfoBeforeMemo.Width + WidthOffset;

  WizardForm.SelectDirLabel.Width:=WizardForm.SelectDirLabel.Width + WidthOffset;
  WizardForm.SelectDirBrowseLabel.Width:=WizardForm.SelectDirBrowseLabel.Width + WidthOffset;
  WizardForm.DiskSpaceLabel.Top:=WizardForm.DiskSpaceLabel.Top + HeightOffset;
  WizardForm.DirBrowseButton.Left:=WizardForm.DirBrowseButton.Left + HeightOffset;
  WizardForm.DirEdit.Width:=WizardForm.DirEdit.Width + HeightOffset;

  WizardForm.ComponentsDiskSpaceLabel.Top:=WizardForm.ComponentsDiskSpaceLabel.Top + HeightOffset;
  WizardForm.SelectComponentsLabel.Width:=WizardForm.SelectComponentsLabel.Width + WidthOffset;
  WizardForm.ComponentsList.Height:=WizardForm.ComponentsList.Height + HeightOffset;
  WizardForm.ComponentsList.Width:=WizardForm.ComponentsList.Width + WidthOffset;
  WizardForm.SelectComponentsLabel.Top:=0;

  WizardForm.ReadyLabel.Width:=WizardForm.ReadyLabel.Width + WidthOffset;
  WizardForm.ReadyMemo.Height:=WizardForm.ReadyMemo.Height + HeightOffset;
  WizardForm.ReadyMemo.Width:=WizardForm.ReadyMemo.Width + WidthOffset;

  WizardForm.ProgressGauge.Width:=WizardForm.ProgressGauge.Width + HeightOffset;
  WizardForm.FilenameLabel.Width:=WizardForm.FilenameLabel.Width + HeightOffset;
  WizardForm.StatusLabel.Width:=WizardForm.StatusLabel.Width + HeightOffset;
  
  WizardForm.DiskSpaceLabel.Height:=0;

  WizardForm.LicenseMemo.Height:=255;
  WizardForm.LicenseAcceptedRadio.Top:=300;
  WizardForm.LicenseAcceptedRadio.Width:=184;
  WizardForm.LicenseNotAcceptedRadio.Left:=290;
  WizardForm.LicenseNotAcceptedRadio.Width:=199;
  
  WizardForm.SelectDirLabel.Top:=0;
  WizardForm.SelectDirLabel.Left:=0;
  WizardForm.SelectDirBrowseLabel.Top:=20;
  WizardForm.SelectDirBitmapImage.Top:=50;
  WizardForm.DirEdit.Top:=55;
  WizardForm.DirEdit.Left:=42;
  WizardForm.DirEdit.Width:=377;
  WizardForm.DirBrowseButton.Top:=54;

end;

/// --- /// --- /// --- /// --- /// --- /// --- /// --- /// --- /// --- /// --- /// --- ///

type
  TComponentDesc = record
    Description: String;
    ImageName: String;
    Index: Integer;
  end;

var
  CompDescs: array of TComponentDesc;
  CompDescPanel, CompDescImgPanel: TPanel;
  BackupPanel, BackupPanel1, BackupPanel2, BackupPanel3: TBevel;
  CompDescText: array[1..2] of TLabel;
  CompIndex, LastIndex: Integer;
  CompDescImg: TBitmapImage;

procedure ShowCompDescription(Sender: TObject; X, Y, Index: Integer; Area: TItemArea);
var
  i: Integer;
begin
  if Index = LastIndex then Exit;
  CompIndex := -1;
  for i := 0 to GetArrayLength(CompDescs) -1 do
  begin
    if (CompDescs[i].Index = Index) then
    begin
      CompIndex := i;
      Break;
    end;
  end;
  if (CompIndex >= 0) and (Area = iaItem) then
  begin
    if not FileExists(ExpandConstant('{tmp}\') + CompDescs[CompIndex].ImageName) then
      ExtractTemporaryFile(CompDescs[CompIndex].ImageName);
    CompDescImg.Bitmap.LoadFromFile(ExpandConstant('{tmp}\') + CompDescs[CompIndex].ImageName);
    CompDescImg.Show;

    CompDescText[2].Caption := CompDescs[CompIndex].Description;
    CompDescText[2].Enabled := True;
  end else
  begin
    CompDescText[2].Caption := CustomMessage('ComponentsInfo');
    CompDescText[2].Enabled := False;
    CompDescImg.Hide;
  end;
  LastIndex := Index;
end;

procedure CompListMouseLeave(Sender: TObject);
begin
  CompDescImg.Hide;
  CompDescText[2].Caption := CustomMessage('ComponentsInfo');
  CompDescText[2].Enabled := False;
  LastIndex := -1;
end;

procedure AddCompDescription(AIndex: Integer; ADescription: String; AImageName: String);
var
  i: Integer;
begin
  i := GetArrayLength(CompDescs);
  SetArrayLength(CompDescs, i + 1);
  CompDescs[i].Description := ADescription;
  CompDescs[i].ImageName := AImageName;
  CompDescs[i].Index := AIndex - 1
end;

//====={ �������� }=====\\
procedure InitializeWizard();
begin
  InitializeWizard1(); {�� BackUpPage.iss}
  RedesignWizardForm;
  WindowResize();

begin
  WizardForm.TypesCombo.Hide;
  WizardForm.ComponentsDiskSpaceLabel.Hide;
  WizardForm.ComponentsList.SetBounds(ScaleX(255), ScaleY(20), ScaleX(265), ScaleY(301));
  WizardForm.ComponentsList.OnItemMouseMove:= @ShowCompDescription;
  WizardForm.ComponentsList.OnMouseLeave := @CompListMouseLeave;

  //====={ ����� ������ }=====\\
  BackupPanel := TBevel.Create(WizardForm);
  with BackupPanel do
  begin
    Parent := WizardForm.SelectDirPage;
    SetBounds(ScaleX(0), ScaleY(170), ScaleX(520), ScaleY(148));
    Shape:= bsFrame;
  end;


  //====={ �������� ���� }=====\\
  CompDescImgPanel := TPanel.Create(WizardForm);
  with CompDescImgPanel do
  begin
    Parent := WizardForm.SelectComponentsPage;
    SetBounds(ScaleX(0), ScaleY(20), ScaleX(250), ScaleY(233));  //����� ��������
    BevelInner := bvRaised;
    BeveLouter := bvLowered;
  end;

  CompDescText[1] := TLabel.Create(WizardForm);
  with CompDescText[1] do
  begin
    Parent := CompDescImgPanel;
    SetBounds(ScaleX(5), ScaleY(5), CompDescImgPanel.Width - ScaleX(10), CompDescImgPanel.Height - ScaleY(10));
    AutoSize := False;
    WordWrap := True;
    Enabled := False;
    Caption := CustomMessage('ComponentsImgInfo');
  end;

  CompDescImg := TBitmapImage.Create(WizardForm);
  with CompDescImg do
  begin
    Parent := CompDescImgPanel;
    SetBounds(ScaleX(2), ScaleY(2), CompDescImgPanel.Width - ScaleX(5), CompDescImgPanel.Height - ScaleY(5));
    Stretch := True;
    Hide;
  end;

  //====={ �������� ���� }=====\\
  CompDescPanel := TPanel.Create(WizardForm);
  with CompDescPanel do
  begin
    Parent := WizardForm.SelectComponentsPage;
    SetBounds(ScaleX(0), ScaleY(258), ScaleX(250), ScaleY(64));  //������ �����
    BevelInner := bvRaised;
    BeveLouter := bvLowered;
  end;

  CompDescText[2] := TLabel.Create(WizardForm);
  with CompDescText[2] do
  begin
    Parent := CompDescPanel;
    SetBounds(ScaleX(5), ScaleY(5), CompDescPanel.Width - ScaleX(10), CompDescPanel.Height - ScaleY(10));
    AutoSize := False;
    WordWrap := True;
    Enabled := False;
    Caption := CustomMessage('ComponentsInfo');
  end;

  //====={ �������� }=====\\
  AddCompDescription(1, CustomMessage('CompDesc1'), 'CompDescImg1.bmp');    // ModPack
  AddCompDescription(2, CustomMessage('CompDesc2'), 'CompDescImg2.bmp');    // Crossfire:
  AddCompDescription(3, CustomMessage('CompDesc3'), 'CompDescImg3.bmp');
  AddCompDescription(4, CustomMessage('CompDesc4'), 'CompDescImg4.bmp');
  AddCompDescription(5, CustomMessage('CompDesc5'), 'CompDescImg5.bmp');
  AddCompDescription(6, CustomMessage('CompDesc6'), 'CompDescImg6.bmp');
  AddCompDescription(7, CustomMessage('CompDesc7'), 'CompDescImg7.bmp');    // UGN
  AddCompDescription(8, CustomMessage('CompDesc8'), 'CompDescImg8.bmp');
  AddCompDescription(9, CustomMessage('CompDesc9'), 'CompDescImg9.bmp');
  AddCompDescription(10, CustomMessage('CompDesc10'), 'CompDescImg10.bmp');
  AddCompDescription(11, CustomMessage('CompDesc11'), 'CompDescImg11.bmp'); // ---
  AddCompDescription(12, CustomMessage('CompDesc12'), 'CompDescImg12.bmp');
  AddCompDescription(13, CustomMessage('CompDesc13'), 'CompDescImg13.bmp');
  AddCompDescription(14, CustomMessage('CompDesc14'), 'CompDescImg14.bmp');
  AddCompDescription(15, CustomMessage('CompDesc15'), 'CompDescImg15.bmp');
  AddCompDescription(16, CustomMessage('CompDesc16'), 'CompDescImg16.bmp');
  AddCompDescription(17, CustomMessage('CompDesc17'), 'CompDescImg17.bmp');
  AddCompDescription(18, CustomMessage('CompDesc18'), 'CompDescImg18.bmp');
  AddCompDescription(19, CustomMessage('CompDesc19'), 'CompDescImg19.bmp');
  AddCompDescription(20, CustomMessage('CompDesc20'), 'CompDescImg20.bmp');
  AddCompDescription(21, CustomMessage('CompDesc21'), 'CompDescImg21.bmp');
  AddCompDescription(22, CustomMessage('CompDesc22'), 'CompDescImg22.bmp');
  AddCompDescription(23, CustomMessage('CompDesc23'), 'CompDescImg23.bmp');
  AddCompDescription(24, CustomMessage('CompDesc24'), 'CompDescImg24.bmp');
  AddCompDescription(25, CustomMessage('CompDesc25'), 'CompDescImg25.bmp');
  AddCompDescription(26, CustomMessage('CompDesc26'), 'CompDescImg26.bmp');
  AddCompDescription(27, CustomMessage('CompDesc27'), 'CompDescImg27.bmp');
  AddCompDescription(28, CustomMessage('CompDesc28'), 'CompDescImg28.bmp');
  AddCompDescription(29, CustomMessage('CompDesc29'), 'CompDescImg29.bmp');
  AddCompDescription(30, CustomMessage('CompDesc30'), 'CompDescImg30.bmp');
  AddCompDescription(31, CustomMessage('CompDesc31'), 'CompDescImg31.bmp');
  AddCompDescription(32, CustomMessage('CompDesc32'), 'CompDescImg32.bmp');
  AddCompDescription(33, CustomMessage('CompDesc33'), 'CompDescImg33.bmp');
  AddCompDescription(34, CustomMessage('CompDesc34'), 'CompDescImg34.bmp');
  AddCompDescription(35, CustomMessage('CompDesc35'), 'CompDescImg35.bmp');
  AddCompDescription(36, CustomMessage('CompDesc36'), 'CompDescImg36.bmp');
  AddCompDescription(37, CustomMessage('CompDesc37'), 'CompDescImg37.bmp');
  AddCompDescription(38, CustomMessage('CompDesc38'), 'CompDescImg38.bmp');
  AddCompDescription(39, CustomMessage('CompDesc39'), 'CompDescImg39.bmp');
  AddCompDescription(40, CustomMessage('CompDesc40'), 'CompDescImg40.bmp');
  AddCompDescription(41, CustomMessage('CompDesc41'), 'CompDescImg41.bmp');
  AddCompDescription(42, CustomMessage('CompDesc42'), 'CompDescImg42.bmp');
  AddCompDescription(43, CustomMessage('CompDesc43'), 'CompDescImg43.bmp');
  AddCompDescription(44, CustomMessage('CompDesc44'), 'CompDescImg44.bmp');
  AddCompDescription(45, CustomMessage('CompDesc45'), 'CompDescImg45.bmp');
  AddCompDescription(46, CustomMessage('CompDesc46'), 'CompDescImg46.bmp');
  AddCompDescription(47, CustomMessage('CompDesc47'), 'CompDescImg47.bmp');
  AddCompDescription(48, CustomMessage('CompDesc48'), 'CompDescImg48.bmp');
  AddCompDescription(49, CustomMessage('CompDesc49'), 'CompDescImg49.bmp');
  AddCompDescription(50, CustomMessage('CompDesc50'), 'CompDescImg50.bmp');
  AddCompDescription(51, CustomMessage('CompDesc51'), 'CompDescImg51.bmp');
  AddCompDescription(52, CustomMessage('CompDesc52'), 'CompDescImg52.bmp');
  AddCompDescription(53, CustomMessage('CompDesc53'), 'CompDescImg53.bmp');
  AddCompDescription(54, CustomMessage('CompDesc54'), 'CompDescImg54.bmp');
  AddCompDescription(55, CustomMessage('CompDesc55'), 'CompDescImg55.bmp');
  AddCompDescription(56, CustomMessage('CompDesc56'), 'CompDescImg56.bmp');
  AddCompDescription(57, CustomMessage('CompDesc57'), 'CompDescImg57.bmp');
  AddCompDescription(58, CustomMessage('CompDesc58'), 'CompDescImg58.bmp');
  AddCompDescription(59, CustomMessage('CompDesc59'), 'CompDescImg59.bmp');
  AddCompDescription(60, CustomMessage('CompDesc60'), 'CompDescImg60.bmp');
  AddCompDescription(61, CustomMessage('CompDesc61'), 'CompDescImg61.bmp');
  AddCompDescription(62, CustomMessage('CompDesc62'), 'CompDescImg62.bmp');
  AddCompDescription(63, CustomMessage('CompDesc63'), 'CompDescImg63.bmp');
  AddCompDescription(64, CustomMessage('CompDesc64'), 'CompDescImg64.bmp');
  AddCompDescription(65, CustomMessage('CompDesc65'), 'CompDescImg65.bmp');
  AddCompDescription(66, CustomMessage('CompDesc66'), 'CompDescImg66.bmp');
  AddCompDescription(67, CustomMessage('CompDesc67'), 'CompDescImg67.bmp');
  AddCompDescription(68, CustomMessage('CompDesc68'), 'CompDescImg68.bmp');
  AddCompDescription(69, CustomMessage('CompDesc69'), 'CompDescImg69.bmp');
  AddCompDescription(70, CustomMessage('CompDesc70'), 'CompDescImg70.bmp');
  AddCompDescription(71, CustomMessage('CompDesc71'), 'CompDescImg71.bmp');
  AddCompDescription(72, CustomMessage('CompDesc72'), 'CompDescImg72.bmp');
  AddCompDescription(73, CustomMessage('CompDesc73'), 'CompDescImg73.bmp');
  AddCompDescription(74, CustomMessage('CompDesc74'), 'CompDescImg74.bmp');
  AddCompDescription(75, CustomMessage('CompDesc75'), 'CompDescImg75.bmp');
  AddCompDescription(76, CustomMessage('CompDesc76'), 'CompDescImg76.bmp');
  AddCompDescription(77, CustomMessage('CompDesc77'), 'CompDescImg77.bmp');
  AddCompDescription(78, CustomMessage('CompDesc78'), 'CompDescImg78.bmp');
  AddCompDescription(79, CustomMessage('CompDesc79'), 'CompDescImg79.bmp');
  AddCompDescription(80, CustomMessage('CompDesc80'), 'CompDescImg80.bmp');
  AddCompDescription(81, CustomMessage('CompDesc81'), 'CompDescImg81.bmp');
  AddCompDescription(82, CustomMessage('CompDesc82'), 'CompDescImg82.bmp');
  AddCompDescription(83, CustomMessage('CompDesc83'), 'CompDescImg83.bmp');
  AddCompDescription(84, CustomMessage('CompDesc84'), 'CompDescImg84.bmp');
  AddCompDescription(85, CustomMessage('CompDesc85'), 'CompDescImg85.bmp');
  AddCompDescription(86, CustomMessage('CompDesc86'), 'CompDescImg86.bmp');
  AddCompDescription(87, CustomMessage('CompDesc87'), 'CompDescImg87.bmp');
  AddCompDescription(88, CustomMessage('CompDesc88'), 'CompDescImg88.bmp');
  AddCompDescription(89, CustomMessage('CompDesc89'), 'CompDescImg89.bmp');
  AddCompDescription(90, CustomMessage('CompDesc90'), 'CompDescImg90.bmp');
  AddCompDescription(91, CustomMessage('CompDesc91'), 'CompDescImg91.bmp');
  AddCompDescription(92, CustomMessage('CompDesc92'), 'CompDescImg92.bmp');
  AddCompDescription(93, CustomMessage('CompDesc93'), 'CompDescImg93.bmp');
  AddCompDescription(94, CustomMessage('CompDesc94'), 'CompDescImg94.bmp');
  AddCompDescription(95, CustomMessage('CompDesc95'), 'CompDescImg95.bmp');
  AddCompDescription(96, CustomMessage('CompDesc96'), 'CompDescImg96.bmp');
  AddCompDescription(97, CustomMessage('CompDesc97'), 'CompDescImg97.bmp');
  AddCompDescription(98, CustomMessage('CompDesc98'), 'CompDescImg98.bmp');
  AddCompDescription(99, CustomMessage('CompDesc99'), 'CompDescImg99.bmp');
  end;
end;
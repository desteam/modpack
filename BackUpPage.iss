#include "SHFileOperation.iss"

[Code]
var
  BackupPage: TWizardPage;
  DeleteRadioButton, MoveRadioButton, NoneRadioButton: TNewRadioButton;

const
  BackupDescText =
    '�� ��������� ������� ������������� � ���������� ����� �������������, ������������� ������� ��� ������������� � ������� �������.' #13#13 '�������� ����������� ����� �� ��������������. ��� ���� ����� ������� ���������� ����� "/res_mods" ��� ������� ��������� ����� ����� �����������.';
  DeleteDescText =
    '���������� ������ ��� ����� � ������.';
  MoveDescText =
    '���������� ������� ��������� ����� ���� ������ � ������.';
  NoneDescText =
    '���������� ������� �� ��� ����.';

function BackupCheck_1(): Boolean;
var
  BackupDescLabel, DeleteDescLabel, MoveDescLabel, NoneDescLabel: TLabel;
begin
  BackupDescLabel:=TLabel.Create(WizardForm);
  BackupDescLabel.Parent:=WizardForm.SelectDirPage;
  BackupDescLabel.Left:=0;
  BackupDescLabel.Top:=95;
  BackupDescLabel.Width:=520;
  BackupDescLabel.Height:=400;
  BackupDescLabel.AutoSize:=False;
  BackupDescLabel.Wordwrap:=True;
  BackupDescLabel.Caption:=BackupDescText;


  DeleteRadioButton:=TNewRadioButton.Create(WizardForm);
  DeleteRadioButton.Parent:=WizardForm.SelectDirPage;
  DeleteRadioButton.Checked:=False;
  DeleteRadioButton.Top:=182;
  DeleteRadioButton.Left:=18;
  DeleteRadioButton.Width:=117;
  DeleteRadioButton.Font.Style:=[fsBold];
  DeleteRadioButton.Font.Size:=9;
  DeleteRadioButton.Caption:='������� �����'
  DeleteDescLabel:=TLabel.Create(WizardForm);
  DeleteDescLabel.Parent:=WizardForm.SelectDirPage;
  DeleteDescLabel.Left:=35;
  DeleteDescLabel.Top:=DeleteRadioButton.Top + DeleteRadioButton.Height + 4;
  DeleteDescLabel.Width:=220;
  DeleteDescLabel.Height:=40;
  DeleteDescLabel.AutoSize:=False;
  DeleteDescLabel.Wordwrap:=True;
  DeleteDescLabel.Caption:=DeleteDescText;

  MoveRadioButton:=TNewRadioButton.Create(WizardForm);
  MoveRadioButton.Parent:=WizardForm.SelectDirPage;
  MoveRadioButton.Checked:=True;
  MoveRadioButton.Top:=227;
  MoveRadioButton.Left:=18;
  MoveRadioButton.Width:=240;
  MoveRadioButton.Font.Style:=[fsBold];
  MoveRadioButton.Font.Size:=9;
  MoveRadioButton.Caption:='������� ��������� ����� ������'
  MoveDescLabel:=TLabel.Create(WizardForm);
  MoveDescLabel.Parent:=WizardForm.SelectDirPage;
  MoveDescLabel.Left:=35;
  MoveDescLabel.Top:=MoveRadioButton.Top + MoveRadioButton.Height + 4;
  MoveDescLabel.Width:=325;
  MoveDescLabel.Height:=40;
  MoveDescLabel.AutoSize:=False;
  MoveDescLabel.Wordwrap:=True;
  MoveDescLabel.Caption:=MoveDescText;

  NoneRadioButton:=TNewRadioButton.Create(WizardForm);
  NoneRadioButton.Parent:=WizardForm.SelectDirPage;
  NoneRadioButton.Checked:=False;
  NoneRadioButton.Top:=272;
  NoneRadioButton.Left:=18;
  NoneRadioButton.Width:=134;
  NoneRadioButton.Font.Style:=[fsBold];
  NoneRadioButton.Font.Size:=9;
  NoneRadioButton.Caption:='������ �� �������'
  NoneDescLabel:=TLabel.Create(WizardForm);
  NoneDescLabel.Parent:=WizardForm.SelectDirPage;
  NoneDescLabel.Left:=35;
  NoneDescLabel.Top:=NoneRadioButton.Top + NoneRadioButton.Height + 4;
  NoneDescLabel.Width:=180;
  NoneDescLabel.Height:=40;
  NoneDescLabel.AutoSize:=False;
  NoneDescLabel.Wordwrap:=True;
  NoneDescLabel.Caption:=NoneDescText;
end;

function BackupCheck_2(): Boolean;
begin
  If DeleteRadioButton.Checked then
begin
  DelTree(ExpandConstant('{app}\res_mods\{#Patch}'), True, True, True);
  DelTree(ExpandConstant('{app}\res_mods\xvm'), True, True, True);
  DelTree(ExpandConstant('{app}\res_mods\ModSettings'), True, True, True);
  DelTree(ExpandConstant('{app}\DeSeRtod ModPack\tmp'), True, True, True);
  DelTree(ExpandConstant('{app}\DeSeRtod ModPack\WoT Tweaker 0.9.5.exe'), True, True, True);
  CreateDir(ExpandConstant('{app}\res_mods\{#Patch}'));
end;
end;

function BackupCheck_3(): Boolean;
begin
  If MoveRadioButton.Checked then
begin
  MoveDir(ExpandConstant('{app}\res_mods\{#Patch}\'),ExpandConstant('{app}\DeSeRtod ModPack\backup\{#Patch}'));
  MoveDir(ExpandConstant('{app}\res_mods\xvm\'),ExpandConstant('{app}\DeSeRtod ModPack\backup\xvm'));
  MoveDir(ExpandConstant('{app}\res_mods\ModSettings\'),ExpandConstant('{app}\DeSeRtod ModPack\backup\ModSettings'));
  CreateDir(ExpandConstant('{app}\res_mods\{#Patch}'));
end;
end;

//**************

function CreateLog(): Boolean;
var A: Integer;
		B: Integer;
		FileName, DateTime: String;
begin
	Result:=True;
	DateTime:=GetDateTimeString('dd/mm/yyyy hh:nn', '-', '-');
	FileName:=ExpandConstant('{app}\DeSeRtod ModPack\' + DateTime + '.log');
	SaveStringToFile(FileName, 'DeSeRtod_ModPack_{#VerPatch}_v_{#Version}' + #13#10 + 'TODAY: ' + DateTime + #13#10 + 'MODS: ' + #13#10, True);
	for A:=0 to WizardForm.ComponentsList.Items.Count - 1 do
		if WizardForm.ComponentsList.Checked[A] then
			begin
			SaveStringToFile(FileName, '+ ' + WizardForm.ComponentsList.ItemCaption[A] + #13#10, True);
			end;
	for B:=0 to WizardForm.TasksList.Items.Count - 1 do
		if WizardForm.TasksList.Checked[B] then
			begin
			SaveStringToFile(FileName, '+ ' + WizardForm.TasksList.ItemCaption[B] + #13#10, True);
			end;
end;

function GetURL(): Boolean;
var WinHttpReq: Variant;
		MyURL:string;
		B: Integer;
  	modsCount:integer;
begin
  modsCount:=0;

	for B:=0 to WizardForm.ComponentsList.Items.Count - 1 do
		if WizardForm.ComponentsList.Checked[B] then
			begin
   		modsCount:=modsCount+1;
			end;


	MyURL:='http://desertod.ru/modpackCount.php?count=' + IntToStr(modsCount);
	WinHttpReq := CreateOleObject('WinHttp.WinHttpRequest.5.1');
	WinHttpReq.Open('GET', MyURL, false);
	WinHttpReq.SetRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	WinHttpReq.Send('');
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
  If CurStep=ssInstall then
		begin
  		BackupCheck_2();
  		BackupCheck_3();
		end;
  If CurStep=ssInstall then
		begin
      CreateLog();
      GetURL();
		end;
end;

//------------------------------------------------------------*********************

procedure InitializeWizard1();
begin
  BackupCheck_1();
end;
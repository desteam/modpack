[Types]
Name: recom; Description: �������������; Flags: iscustom

[Components]
Name: DES; Description: DeSeRtod Mod Pack; Flags: disablenouninstallwarning;

// CROSSFIRE - 5
Name: DES\PRIC; Description: �������:; Flags: disablenouninstallwarning
Name: DES\PRIC\DES; Description: ������� ������ DeSeRtod`a; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\PRIC\JOV; Description: ������ Jove`a; Flags: exclusive disablenouninstallwarning
Name: DES\PRIC\VSP; Description: ������ Vspishk`�; Flags: exclusive disablenouninstallwarning
Name: DES\PRIC\MUR; Description: ������ Murazor`�; Flags: exclusive disablenouninstallwarning
Name: DES\PRIC\MELT; Description: ������ �� MeltyMap; Flags: exclusive disablenouninstallwarning
Name: DES\PRIC\DAM; Description: �������� ���; Flags: exclusive disablenouninstallwarning

// UGN - 5
Name: DES\UGN; Description: ���:; Flags: disablenouninstallwarning;
Name: DES\UGN\POLUKRUG; Description: ����������; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\UGN\BIG_POLUKRUG; Description: ������� ����������; Flags: exclusive disablenouninstallwarning
Name: DES\UGN\ANGLE; Description: ��������; Flags: exclusive disablenouninstallwarning
Name: DES\UGN\SKOB; Description: ������������� ������; Flags: exclusive disablenouninstallwarning

// DAMAGE PANEL - 4
Name: DES\DAMAGE; Description: ����� ������:; Flags: disablenouninstallwarning
Name: DES\DAMAGE\GAM; Description: �� GambitER; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\DAMAGE\DEL; Description: �� Dellux; Flags: exclusive disablenouninstallwarning
Name: DES\DAMAGE\ZAY; Description: �� Zayaz; Flags: exclusive disablenouninstallwarning

// INFOPANEL - 5
Name: DES\INFOPANEL; Description: �������������� ������:; Flags: disablenouninstallwarning
Name: DES\INFOPANEL\MIN; Description: ���������������; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\INFOPANEL\EXT; Description: ���������; Flags: exclusive disablenouninstallwarning
Name: DES\INFOPANEL\EXT_C; Description: ��������� (�������); Flags: exclusive disablenouninstallwarning
Name: DES\INFOPANEL\BIG; Description: �������; Flags: exclusive disablenouninstallwarning

// OTHER - 8
Name: DES\BATTLEGUI; Description: ������ ���������:; Flags: disablenouninstallwarning
Name: DES\BATTLEGUI\WN8; Description: ����������� WN8 � ���; Flags: disablenouninstallwarning
Name: DES\BATTLEGUI\ATTACK; Description: ��������� ����������� �����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\BATTLEGUI\GUNS; Description: ����������� ������� �� ����-�����; Flags: disablenouninstallwarning
Name: DES\BATTLEGUI\HP; Description: ��������� ���������� ��; Flags: disablenouninstallwarning
Name: DES\BATTLEGUI\SPG; Description: ������������� ����; Flags: disablenouninstallwarning
//Name: DES\BATTLEGUI\CURS; Description: ��������� ����������; Flags: disablenouninstallwarning
Name: DES\BATTLEGUI\SPOT; Description: ��������� � �������; Flags: disablenouninstallwarning
Name: DES\BATTLEGUI\TRACK; Description: ������ ������ ����� ��������; Flags: disablenouninstallwarning

// SOUNDS - 6
Name: DES\SOUNDS; Description: �������� �����������:; Flags: disablenouninstallwarning
Name: DES\SOUNDS\CRIT; Description: �������� �����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\SOUNDS\LAMP; Description: ������� ��������; Flags: disablenouninstallwarning
Name: DES\SOUNDS\LAMP\LOW; Description: �����; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\SOUNDS\LAMP\HIGH; Description: �������; Flags: exclusive disablenouninstallwarning
Name: DES\SOUNDS\LAMP\TIMER; Description: ������ 10 ������; Flags: disablenouninstallwarning; Types: recom;

// TANKS - 4
Name: DES\TANKS; Description: ������ ������:; Flags: disablenouninstallwarning
Name: DES\TANKS\TRUPS; Description: ����� ����� ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\TANKS\TRACKS; Description: ����� ������ �����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\TANKS\COL; Description: ������� �������� � ����������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\TANKS\SKINS; Description: ������ � ������ ��������; Flags: disablenouninstallwarning
Name: DES\TANKS\SKINS\100; Description: ������; Flags: exclusive disablenouninstallwarning
Name: DES\TANKS\SKINS\50; Description: ������ �� 50%; Flags: exclusive disablenouninstallwarning
//Name: DES\TANKS\LASTSPOT; Description: ������ ������������ �����; Flags: disablenouninstallwarning

// HANGARS - 4
Name: DES\ALT; Description: �������������� ������:; Flags: disablenouninstallwarning
Name: DES\ALT\DES; Description: ������� ����� DeSeRtod`a; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\ALT\HELL; Description: ��������������� ������; Flags: exclusive disablenouninstallwarning
Name: DES\ALT\MAP; Description: ����� ����; Flags: exclusive disablenouninstallwarning
Name: DES\ALT\MIKU; Description: �����-����� �Miku�; Flags: exclusive disablenouninstallwarning
Name: DES\ALT\SQU; Description: ��������� �������; Flags: exclusive disablenouninstallwarning
Name: DES\ALT\RUIN; Description: ������ ����������; Flags: exclusive disablenouninstallwarning
Name: DES\ALT\NY; Description: ����������; Flags: exclusive disablenouninstallwarning

// WAITLOGO - 3
Name: DES\WAITLOGO; Description: ������ ��������:; Flags: disablenouninstallwarning
Name: DES\WAITLOGO\DES; Description: ������ DeSeRtod'a; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\WAITLOGO\KRAB; Description: ������ � ������; Flags: exclusive disablenouninstallwarning

// OTHER - 6
Name: DES\HANG; Description: ����������� ������:; Flags: disablenouninstallwarning

Name: DES\HANG\PERK; Description: ����������� �������� ������; Flags: disablenouninstallwarning
Name: DES\HANG\TREE; Description: ������������ ������ ��������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\HANG\BAN; Description: ����������� �����-������; Flags: disablenouninstallwarning
Name: DES\HANG\WGFM; Description: ����� WarGaming FM; Flags: disablenouninstallwarning
Name: DES\HANG\WGSOC; Description: WGSocial; Flags: disablenouninstallwarning
Name: DES\HANG\REPL; Description: �������� �������; Flags: disablenouninstallwarning


//PMOD - 31(-8)
Name: DES\PMOD; Description: ����� � ���������:; Flags: disablenouninstallwarning;

Name: DES\PMOD\0; Description: ���������� ����������:; Flags: disablenouninstallwarning
Name: DES\PMOD\0\1; Description: �������; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\PMOD\0\2; Description: ���������������; Flags: exclusive disablenouninstallwarning
Name: DES\PMOD\0\3; Description: ��������������� (��������������); Flags: exclusive disablenouninstallwarning
Name: DES\PMOD\0\4; Description: ���������; Flags: exclusive disablenouninstallwarning
Name: DES\PMOD\0\SES; Description: ������ �� ������; Flags: disablenouninstallwarning

Name: DES\PMOD\1; Description: ��������� �������� ��� � ����; Flags: disablenouninstallwarning
;Name: DES\PMOD\1\1; Description: �������; Flags: exclusive disablenouninstallwarning
;Name: DES\PMOD\1\2; Description: ���������; Flags: exclusive disablenouninstallwarning; Types: recom;
;Name: DES\PMOD\1\3; Description: ���������������; Flags: exclusive disablenouninstallwarning
;Name: DES\PMOD\1\4; Description: ��������������� (��������������); Flags: exclusive disablenouninstallwarning

Name: DES\PMOD\17; Description: ���������� ���; Flags: disablenouninstallwarning
;Name: DES\PMOD\17\1; Description: ��������� (med); Flags: exclusive disablenouninstallwarning; Types: recom;
;Name: DES\PMOD\17\2; Description: �� Armagomen`a; Flags: exclusive disablenouninstallwarning
;Name: DES\PMOD\17\3; Description: ����������� (def); Flags: exclusive disablenouninstallwarning
;Name: DES\PMOD\17\4; Description: ��������������� (dem); Flags: exclusive disablenouninstallwarning

Name: DES\PMOD\2; Description: ���������� ������� �������; Flags: disablenouninstallwarning
Name: DES\PMOD\3; Description: ����������� ��������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\4; Description: ������� ��������� ���������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\5; Description: �������������� ������������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\6; Description: ���������� ��������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\7; Description: ���������� ������� ����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\8; Description: ��������� ����������� ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\9; Description: ��������� ������ ����� ������; Flags: disablenouninstallwarning
Name: DES\PMOD\10; Description: NoScroll; Flags: disablenouninstallwarning
Name: DES\PMOD\11; Description: ��������� ������ ������ ��-���; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\12; Description: ��������� ������ (F3); Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\13; Description: ������������ ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\14; Description: ������������ ���; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\15; Description: ��������� ����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\16; Description: ���������� ������������ ��������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\PMOD\17; Description: ��������� ������; Flags: disablenouninstallwarning;
Name: DES\PMOD\18; Description: ����������� ����� �����; Flags: disablenouninstallwarning; Types: recom;


// XVM - 17
Name: DES\XVM; Description: ����������� ��� XVM:; Flags: disablenouninstallwarning
Name: DES\XVM\1; Description: ���������� ����� �� ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\2; Description: ���������� ������ �������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\3; Description: ���������� �������� ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\3\2; Description: � 2 ����; Flags: disablenouninstallwarning exclusive
Name: DES\XVM\3\1; Description: � 1 ���; Flags: disablenouninstallwarning exclusive
Name: DES\XVM\4; Description: ���� � ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\5; Description: ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\5\1; Description: � ���� ������; Flags: disablenouninstallwarning
Name: DES\XVM\6; Description: ���������� ����-�����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\6\1; Description: ������� ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\7; Description: ���������� ����-�����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\8; Description: ��������� ������ �����; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\9; Description: ���������� �������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\10; Description: ���������� ������� � ���; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\11; Description: ���� � ������; Flags: disablenouninstallwarning; Types: recom;
Name: DES\XVM\12; Description: ��������� �����; Flags: disablenouninstallwarning
Name: DES\XVM\12\1; Description: DeSeRtod �����; Flags: exclusive disablenouninstallwarning; Types: recom;
Name: DES\XVM\12\2; Description: ���� DeSeRtod`a; Flags: exclusive disablenouninstallwarning
Name: DES\XVM\12\3; Description: ������ ��������; Flags: exclusive disablenouninstallwarning
Name: DES\XVM\12\4; Description: ������� ������; Flags: exclusive disablenouninstallwarning
Name: DES\XVM\12\5; Description: ��������; Flags: exclusive disablenouninstallwarning
Name: DES\XVM\12\6; Description: ������� �������; Flags: exclusive disablenouninstallwarning
Name: DES\XVM\12\7; Description: ����; Flags: exclusive disablenouninstallwarning
Name: DES\XVM\13; Description: ���������� ������� � ����; Flags: disablenouninstallwarning; Types: recom;

Name: DES\TWEAK; Description: WoT Tweaker 0.9.5 DeSeRtod Edition; Flags: disablenouninstallwarning
//Name: DES\UPDATE; Description: ����������� � ������ ����� ������; Flags: disablenouninstallwarning















[Files]

Source: "MODS\7Z\*"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall; Components: DES;

// CROSSFIRE
Source: "MODS\CROSSFIRE\DES\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PRIC\DES;
Source: "MODS\CROSSFIRE\JOV\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PRIC\JOV;
Source: "MODS\CROSSFIRE\VSP\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PRIC\VSP;
Source: "MODS\CROSSFIRE\MUZ\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PRIC\MUR;
Source: "MODS\CROSSFIRE\MELT\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PRIC\MELT;
Source: "MODS\CROSSFIRE\DAM\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PRIC\DAM;

// UGN
Source: "MODS\UGN\BASE\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\UGN;
Source: "MODS\UGN\POLUKRUG\*"; DestDir: "{app}\res_mods\{#Patch}\gui\scaleform\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\UGN\POLUKRUG;
Source: "MODS\UGN\BIG_POLUKRUG\*"; DestDir: "{app}\res_mods\{#Patch}\gui\scaleform\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\UGN\BIG_POLUKRUG;
Source: "MODS\UGN\ANGLE\*"; DestDir: "{app}\res_mods\{#Patch}\gui\scaleform\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\UGN\ANGLE;
Source: "MODS\UGN\SKOB\*"; DestDir: "{app}\res_mods\{#Patch}\gui\scaleform\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\UGN\SKOB;

// DAMAGE PANEL
Source: "MODS\DAMAGE_PANEL\GAM\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\DAMAGE\GAM;
Source: "MODS\DAMAGE_PANEL\DEL\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\DAMAGE\DEL;
Source: "MODS\DAMAGE_PANEL\ZAY\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\DAMAGE\ZAY;

// INFOPANEL
Source: "MODS\INFOPANEL\MIN\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\INFOPANEL\MIN;
Source: "MODS\INFOPANEL\EXT\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\INFOPANEL\EXT;
Source: "MODS\INFOPANEL\EXT_C\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\INFOPANEL\EXT_C;
Source: "MODS\INFOPANEL\BIG\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\INFOPANEL\BIG;

// OTHER
Source: "MODS\BATTLEGUI\WN8\*"; DestDir: "{app}\res_mods\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\WN8;
Source: "MODS\BATTLEGUI\ATTACK\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\ATTACK;
Source: "MODS\BATTLEGUI\GUNS\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\GUNS;
Source: "MODS\BATTLEGUI\HP\*"; DestDir: "{app}\res_mods\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\HP;
Source: "MODS\BATTLEGUI\SPG\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\SPG;
//Source: "MODS\BATTLEGUI\RUINBERG\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\RUINBERG;
Source: "MODS\BATTLEGUI\SPOT\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\SPOT;
Source: "MODS\BATTLEGUI\TRACK\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\BATTLEGUI\TRACK;

// SOUNDS
Source: "{app}\res\audio\*"; DestDir: "{app}\res_mods\{#Patch}\audio\"; Flags: external recursesubdirs skipifsourcedoesntexist; Components: DES\SOUNDS;
Source: "MODS\SOUNDS\CRIT\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\SOUNDS\CRIT;
Source: "MODS\SOUNDS\LOW\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\SOUNDS\LAMP\LOW;
Source: "MODS\SOUNDS\HIGH\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\SOUNDS\LAMP\HIGH;
Source: "MODS\SOUNDS\LOW_TIME\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\SOUNDS\LAMP\LOW and DES\SOUNDS\LAMP\TIMER;
Source: "MODS\SOUNDS\HIGH_TIME\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\SOUNDS\LAMP\HIGH and DES\SOUNDS\LAMP\TIMER;

// TANKS
Source: "MODS\TANKS\TRUPS\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\TANKS\TRUPS;
Source: "MODS\TANKS\TRACKS\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\TANKS\TRACKS;
Source: "MODS\TANKS\COL\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\TANKS\COL;
Source: "{tmp}\models_SKIN100.7z"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall external; Components: DES\TANKS\SKINS\100; AfterInstall: ExtractZIP;
Source: "{tmp}\models_SKIN50.7z"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall external; Components: DES\TANKS\SKINS\50; AfterInstall: ExtractZIP;
//Source: "MODS\HANG\LASTSPOT\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\TANKS\LASTSPOT;


// HANGARS
//Source: "{tmp}\murazor.7z"; DestDir: "{app}\DeSeRtod Mod Pack\mods"; Flags: ignoreversion deleteafterinstall external; Components: dmp\pricels\murazor; AfterInstall: ExtractResources
Source: "MODS\HANGARS\DES\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\ALT\DES;
Source: "MODS\HANGARS\HELL\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\ALT\HELL;
Source: "{tmp}\hang_MAP.7z"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall external; Components: DES\ALT\MAP; AfterInstall: ExtractZIP;
Source: "{tmp}\hang_MIKU.7z"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall external; Components: DES\ALT\MIKU; AfterInstall: ExtractZIP;
Source: "{tmp}\hang_SQUARE.7z"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall external; Components: DES\ALT\SQU; AfterInstall: ExtractZIP;
Source: "{tmp}\hang_OLD.7z"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall external; Components: DES\ALT\RUIN; AfterInstall: ExtractZIP;
Source: "{tmp}\hang_NY.7z"; DestDir: "{app}\DeSeRtod ModPack\tmpmods\"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall external; Components: DES\ALT\NY; AfterInstall: ExtractZIP;
// desertod.ru/MODS/MAP.7z

// WAITLOGO
Source: "MODS\WAITLOGO\DES\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\WAITLOGO\DES;
Source: "MODS\WAITLOGO\KRAB\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\WAITLOGO\KRAB;

// OTHER
Source: "{app}\res\text\*"; DestDir: "{app}\res_mods\{#Patch}\text\"; Flags: external recursesubdirs skipifsourcedoesntexist; Components: DES\HANG\PERK;
Source: "MODS\HANG\PERK\*"; DestDir: "{app}\res_mods\{#Patch}\text\LC_MESSAGES\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\HANG\PERK;
Source: "MODS\HANG\TREE\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\HANG\TREE;
Source: "MODS\HANG\BAN\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\HANG\BAN;
Source: "MODS\HANG\WGFM\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\HANG\WGFM;
Source: "MODS\HANG\WGSOC\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\HANG\WGSOC;
Source: "MODS\HANG\REPLAY\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\HANG\REPL;

// PMOD
Source: "MODS\PMOD\MOD\*"; DestDir: "{app}\res_mods\{#Patch}\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD;

Source: MODS\PMOD\CONFIG\sessionStatistic.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\0;
Source: MODS\PMOD\CONFIG\session1\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\0;
Source: MODS\PMOD\SKINS\STAT\1\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\0\1;
Source: MODS\PMOD\SKINS\STAT\2\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\0\2;
Source: MODS\PMOD\SKINS\STAT\3\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\0\3;
Source: MODS\PMOD\SKINS\STAT\4\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\0\4;
Source: MODS\PMOD\CONFIG\session2\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\0\SES;

Source: MODS\PMOD\CONFIG\battleChat.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\1;
Source: MODS\PMOD\CONFIG\systemMessages.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\1;
//Source: MODS\PMOD\SKINS\CHAT\1\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\1\1;
//Source: MODS\PMOD\SKINS\CHAT\2\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\1\2;
//Source: MODS\PMOD\SKINS\CHAT\3\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\1\3;
//Source: MODS\PMOD\SKINS\CHAT\4\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\1\4;

Source: MODS\PMOD\CONFIG\battleChat.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\17;
Source: MODS\PMOD\CONFIG\systemMessages.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\17;
//Source: MODS\PMOD\SKINS\MES\1\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\17\1;
//Source: MODS\PMOD\SKINS\MES\2\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\17\2;
//Source: MODS\PMOD\SKINS\MES\3\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\17\3;
//Source: MODS\PMOD\SKINS\MES\4\*; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\skins"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\17\4;

Source: MODS\PMOD\CONFIG\cameraOptions.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\2;
Source: MODS\PMOD\CONFIG\battleLoading.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\3;
Source: MODS\PMOD\CONFIG\hangar.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\4;
Source: MODS\PMOD\CONFIG\systemMessages.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\4;
Source: MODS\PMOD\CONFIG\horizontalStabilizer.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\5;
Source: MODS\PMOD\CONFIG\loginWindow.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\6;
Source: MODS\PMOD\CONFIG\noBinoculars.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\7;
Source: MODS\PMOD\CONFIG\noDynamic.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\8;
Source: MODS\PMOD\CONFIG\noScroll.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\10;
Source: MODS\PMOD\CONFIG\onStartDeadDistance.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\9;
Source: MODS\PMOD\CONFIG\TDBrakeRemover.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\11;
Source: MODS\PMOD\CONFIG\videomode.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\12;
Source: MODS\PMOD\CONFIG\zoomDistance.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\13;
Source: MODS\PMOD\CONFIG\zoomX.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\14;
Source: MODS\PMOD\CONFIG\zoomIndicator.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\15;
Source: MODS\PMOD\CONFIG\battleGui.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\16;
Source: MODS\PMOD\CONFIG\oldServerCrosshair.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\17;
Source: MODS\PMOD\CONFIG\reducedArmor.json; DestDir: "{app}\res_mods\{#Patch}\scripts\client\mods\PMOD\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\PMOD\18;

// XVM
Source: "MODS\XVM\MOD\*"; DestDir: "{app}\res_mods\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM;

Source: "MODS\XVM\CONF\battleLoading.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\1;
Source: "MODS\XVM\CONF\captureBar.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\2;
Source: "MODS\XVM\CONF\CAROUSEL\2\carousel.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\3\2;
Source: "MODS\XVM\CONF\CAROUSEL\1\carousel.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\3\1;
Source: "MODS\XVM\CONF\hangar.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\4;
Source: "MODS\XVM\CONF\hitLog.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\5;
Source: "MODS\XVM\CONF\HITLOG\hitLog.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\5\1;
Source: "MODS\XVM\CONF\minimap.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\6;
//Source: "MODS\XVM\CONF\minimapAlt.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\6;           - �� ������������
Source: "MODS\XVM\CONF\minimapCircles.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\6;
Source: "MODS\XVM\CONF\minimapLabels.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\6;
Source: "MODS\XVM\CONF\minimapLines.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\6;
Source: "MODS\XVM\CONF\ICONS\minimap.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\6\1;
Source: "MODS\XVM\CONF\hotkeys.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\7;
Source: "MODS\XVM\CONF\login.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\8;
Source: "MODS\XVM\CONF\markers.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\9;
//Source: "MODS\XVM\CONF\markersAliveExtended.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\9; - ��������� � ����� �����
//Source: "MODS\XVM\CONF\markersAliveNormal.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\9;   - ��������� � ����� �����
//Source: "MODS\XVM\CONF\markersDeadExtended.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\9;  - ��������� � ����� �����
//Source: "MODS\XVM\CONF\markersDeadNormal.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\9;    - ��������� � ����� �����
Source: "MODS\XVM\CONF\statisticForm.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\10;
Source: "MODS\XVM\CONF\battleLoading.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\10;
Source: "MODS\XVM\CONF\clock.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\11;
Source: "MODS\XVM\CONF\playersPanel.xc"; DestDir: "{app}\res_mods\xvm\configs\DeSeRtod\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\13;

Source: "MODS\XVM\LAMP\1\*"; DestDir: "{app}\res_mods\xvm\res\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\12\1;
Source: "MODS\XVM\LAMP\2\*"; DestDir: "{app}\res_mods\xvm\res\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\12\2;
Source: "MODS\XVM\LAMP\3\*"; DestDir: "{app}\res_mods\xvm\res\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\12\3;
Source: "MODS\XVM\LAMP\4\*"; DestDir: "{app}\res_mods\xvm\res\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\12\4;
Source: "MODS\XVM\LAMP\5\*"; DestDir: "{app}\res_mods\xvm\res\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\12\5;
Source: "MODS\XVM\LAMP\6\*"; DestDir: "{app}\res_mods\xvm\res\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\12\6;
Source: "MODS\XVM\LAMP\7\*"; DestDir: "{app}\res_mods\xvm\res\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\XVM\12\7;

Source: "MODS\TWEAK\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\TWEAK;
//Source: "MODS\UPDATE\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: DES\UPDATE;